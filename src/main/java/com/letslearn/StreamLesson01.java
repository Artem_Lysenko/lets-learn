package com.letslearn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

class InventoryItem {
    private Long id;
    private String vendor;
    private String name;
    private Long cost;
    private Date suppliedAt;
    private String suppliedBy;

    public InventoryItem(Long id, String vendor, String name, Long cost, Date suppliedAt, String suppliedBy) {
        this.id = id;
        this.vendor = vendor;
        this.name = name;
        this.cost = cost;
        this.suppliedAt = suppliedAt;
        this.suppliedBy = suppliedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Date getSuppliedAt() {
        return suppliedAt;
    }

    public void setSuppliedAt(Date suppliedAt) {
        this.suppliedAt = suppliedAt;
    }

    public String getSuppliedBy() {
        return suppliedBy;
    }

    public void setSuppliedBy(String suppliedBy) {
        this.suppliedBy = suppliedBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


class InventoryItemInfo {
    private String name;
    private Long cost;

    public InventoryItemInfo() {
    }

    public InventoryItemInfo(String name, Long cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }
}

public class StreamLesson01 {

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("dd-LL-yyyy");

        List<InventoryItem> inventoryItems = Arrays.asList(
                new InventoryItem(1L, "Adidas", "Sneakers A", 100L, df.parse("01-12-2019"), "Delivery A"),
                new InventoryItem(2L, "Adidas", "Sneakers B", 80L, df.parse("01-06-2018"), "Delivery A"),
                new InventoryItem(3L, "Adidas", "Sneakers C", 80L, df.parse("03-12-2019"), "Delivery B"),
                new InventoryItem(4L, "Nike", "Sneakers H", 180L, df.parse("15-06-2019"), "Delivery C"),
                new InventoryItem(5L, "Reebok", "Sneakers D", 90L, df.parse("15-06-2018"), "Delivery A")
        );


        // Task 1
        // Найти все товары, которые стоят дешевле 100, отсортировать по названию
        // На выходе необходимо получить List<InventoryItemInfo>


        // Task 2
        // Сгрупировать все товары по производителю, так что бы на выходе получить Map<String, List<InventoryItem>>
        // key - название произодителя


        // Task 3
        // Найти самый дорогой товар
    }

}
